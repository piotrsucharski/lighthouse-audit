const arrayNumbersInRange = (length = 10, start = 1) => [...Array(length)].map((el, idx) => start + idx);
const average = data => data.reduce((sum, value) => sum + value, 0) / data.length;
const standardDeviation = values => {
    const avg = average(values);
    const squareDiffs = values.map(value => {
        const diff = value - avg;
        return diff * diff;
    });

    const avgSquareDiff = average(squareDiffs);

    return Math.sqrt(avgSquareDiff);
};
const getComputedMetrics = (metricArray) => ({
    average: average(metricArray),
    stdDev: standardDeviation(metricArray),
    sampleSize: metricArray.length
});

module.exports = {
    arrayNumbersInRange,
    standardDeviation,
    average,
    getComputedMetrics,
};
