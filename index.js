const util = require('util');
const { ensureDirSync, readFileSync, writeFileSync } = require('fs-extra');
const exec = util.promisify(require('child_process').exec);
const { arrayNumbersInRange, getComputedMetrics } = require('./utils');

//Specify number of audits you want to run by changing first arg in the function call below
const numberOfAudits = arrayNumbersInRange(1000, 1);

const runAudit = (auditIdx) => async () => {
    try {
        return exec(`lighthouse https://app.resumelab.com/ --output json --output-path ${__dirname}/results/audit${auditIdx}.json --emulated-form-factor desktop --throttling-method simulate --chrome-flags="--headless --user-agent='Bold-Lighthouse/1.0'" --budget-path=budget.json`);
    } catch (e) {
        console.log(`audit ${auditIdx} failed to run`);
    }
};

const auditsToRun = numberOfAudits.map(entry => runAudit(entry));

ensureDirSync(`${__dirname}/results`);

(async () => {
    for await(const audit of auditsToRun) {
        await audit();
    }

    const allFirstContentFulPaint = [];
    const allFirstMeaningfulPaint = [];
    const allSpeedIndex = [];
    const allFirstCpuIdle = [];
    const allInteractive = [];
    const allTimeToFirstByte = [];
    const allTotalMainThreadWork = [];
    const allScriptEvaluation = [];
    const allEstimatedInputLatency = [];
    const allOther = [];
    const allStyleLayout = [];
    const allPaintCompositeRender = [];
    const allScriptParseCompile = [];
    const allGarbageCollection = [];
    const allParseHTML = [];

    for (let i = 1; i <= numberOfAudits.length; i++) {
        try {
            const auditFile = JSON.parse(readFileSync(`${__dirname}/results/audit${i}.json`));

            const { firstContentfulPaint, firstMeaningfulPaint, speedIndex, firstCPUIdle, interactive } = auditFile.audits.metrics.details.items[0];
            const totalMainThreadWork = auditFile.audits["mainthread-work-breakdown"].numericValue;
            const timeToFirstByte = auditFile.audits["time-to-first-byte"].numericValue;
            const estimatedInputLatency = auditFile.audits["estimated-input-latency"].numericValue;
            const [
                scriptEvaluation,
                other,
                styleLayout,
                paintCompositeRender,
                scriptParseCompile,
                garbageCollection,
                parseHTML,
            ] = auditFile.audits["mainthread-work-breakdown"].details.items;

            //General timings
            allFirstContentFulPaint.push(firstContentfulPaint);
            allFirstMeaningfulPaint.push(firstMeaningfulPaint);
            allSpeedIndex.push(speedIndex);
            allFirstCpuIdle.push(firstCPUIdle);
            allInteractive.push(interactive);
            allEstimatedInputLatency.push(estimatedInputLatency);

            //Timings breakdown
            allTotalMainThreadWork.push(totalMainThreadWork);
            allScriptEvaluation.push(scriptEvaluation.duration);
            allOther.push(other.duration);
            allStyleLayout.push(styleLayout.duration);
            allPaintCompositeRender.push(paintCompositeRender.duration);
            allScriptParseCompile.push(scriptParseCompile.duration);
            allGarbageCollection.push(garbageCollection.duration);
            allParseHTML.push(parseHTML.duration);
            allTimeToFirstByte.push(timeToFirstByte);
        } catch (e) {
            console.log(`audit ${i} doesn't exist`);
        }
    }


    const computedMetrics = {
        firstContentfulPaint: getComputedMetrics(allFirstContentFulPaint),
        firstMeaningfulPaint: getComputedMetrics(allFirstMeaningfulPaint),
        speedIndex: getComputedMetrics(allSpeedIndex),
        firstCpuIdle: getComputedMetrics(allFirstCpuIdle),
        interactive: getComputedMetrics(allInteractive),
        timeToFirstByte: getComputedMetrics(allTimeToFirstByte),
        estimatedInputLatency: getComputedMetrics(allEstimatedInputLatency),
        timings: {
            totalMainThreadWork: getComputedMetrics(allTotalMainThreadWork),
            scriptEvaluation: getComputedMetrics(allScriptEvaluation),
            other: getComputedMetrics(allOther),
            styleLayout: getComputedMetrics(allStyleLayout),
            paintCompositeRender: getComputedMetrics(allPaintCompositeRender),
            scriptParseCompile: getComputedMetrics(allScriptParseCompile),
            garbageCollection: getComputedMetrics(allGarbageCollection),
            parseHTML: getComputedMetrics(allParseHTML),
        },
        allFirstContentFulPaint,
        allFirstMeaningfulPaint,
        allSpeedIndex,
        allFirstCpuIdle,
        allInteractive,
        allEstimatedInputLatency,
        allTimeToFirstByte,
        allTotalMainThreadWork,
        allScriptEvaluation,
        allOther,
        allStyleLayout,
        allPaintCompositeRender,
        allScriptParseCompile,
        allGarbageCollection,
        allParseHTML,
    };

    try {
        const auditFile = JSON.parse(readFileSync(`${__dirname}/results/audit1.json`));

        computedMetrics.resourceSummary = auditFile.audits["resource-summary"].details.items;
    } catch (e) {
        console.log('reading resource summary failed');
    }

    writeFileSync(`${__dirname}/computedMetrics.json`, JSON.stringify(computedMetrics));
})();
